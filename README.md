 **Task manager**
=====================

### О проекте:
**Приложение для контроля статусов выполняемых задач.**

-----------------------------------
### Ссылка на альтернативный репозиторий в GitHub: [https://github.com/mallet322/jse](https://github.com/mallet322/jse)

### Требования к software:
* Java 11 или выше.
* Maven 3 или выше.

### Запуск приложения:
    cd <расположение проекта>/target/ 
    java -jar task-manager-<версия>.jar <аргументы запуска>
    
## Аргументы запуска приложения:
`help` Отображение списка терминальных команд.

`version` Отображение версии программы.

`about` Отображение информации об разработчике.

`exit` Выход из программы.

### Команды для работы с проектами: 

`project-create` Создание нового проекта.

`project-list` Отображение списка проектов.

`project-view-by-index` Отображение проекта по индексу.

`project-view-by-id` Отображение проекта по ключу.

`project-update-by-index` Изменение проекта по индексу.

`project-update-by-id` Изменение проекта по ключу.

`project-clear` Удаление всех проектов.

`project-remove-by-index` Удаление проекта по индексу.

`project-remove-by-id` Удаление проекта по ключу.

`project-remove-by-name` Удаление проекта по имени.

### Команды для работы с задачами: 

`task-create` Создание новой задачи.

`task-list` Отображение списка задач.

`task-view-by-index` Отображение задачи по индексу.

`task-view-by-id` Отображение задачи по ключу.

`task-update-by-index` Изменение задачи по индексу.

`task-update-by-id` Изменение задачи по ключу.
 
`task-clear` Удаление всех задач.

`task-remove-by-index` Удаление задачи по индексу.

`task-remove-by-id` Удаление задачи по ключу.

`task-remove-by-name` Удаление задачи по имени.

`task-list-by-project-id` Отображение задачи в проекте.

`task-add-to-project-by-ids` Добавление задачи в проект.

`task-remove-from-project-by-ids` Удаление задачи из проекта.

### Команды для работы с пользователями: 

`user-create` Создание нового пользователя.

`user-list` Отображение списка пользователей.

`user-clear` Удаление всех пользователей.

`user-add-information` Добавление пользовательской информации.

`user-view-by-login` Просмотр пользователя по логину.

`user-update-by-login` Изменение пользователя по логину.

`user-remove-by-login` Удаление пользователя по логину.

## Сборка приложения 
```bash
mvn clean install
```
## Разработчик: 
   ***Шокин Илья*** 

   mailto: [shokin_is@nlmk.com](mailto:shokin_is@nlmk.com)