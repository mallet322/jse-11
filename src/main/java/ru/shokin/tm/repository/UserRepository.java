package ru.shokin.tm.repository;

import ru.shokin.tm.constant.Role;
import ru.shokin.tm.entity.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository {

    private final List<User> users = new ArrayList<>();

    public User create(final String login, final String password, final Role role) {
        final User user = new User(login, password, role);
        users.add(user);
        return user;
    }

    public User update(final String login, final String password, Role role,
                       final String firstName, final String lastName) {
        final User user = findByLogin(login);
        if (user == null) return null;
        user.setPassword(password);
        user.setRole(role);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        return user;
    }

    public User addUserInformation(final String login, final String firstName, final String lastName) {
        final User user = findByLogin(login);
        if (user == null) return null;
        user.setFirstName(firstName);
        user.setLastName(lastName);
        return user;
    }

    public List<User> findAll() {
        return users;
    }

    public User findByLogin(final String login) {
        for (final User user : users) {
            if (user.getLogin().equals(login)) return user;
        }
        return null;
    }

    public void clear() {
        users.clear();
    }

    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

}