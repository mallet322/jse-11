package ru.shokin.tm.controller;

import ru.shokin.tm.entity.User;
import ru.shokin.tm.service.UserService;

public class UserController extends AbstractController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    public int createUser() {
        System.out.println("Register for TaskManager");
        System.out.println("Please, enter user name:");
        final String name = scanner.nextLine();
        System.out.println("Please, enter user password:");
        final String password = scanner.nextLine();
        final String role = grantRoles();
        final User user = userService.create(name, password, role);
        if (user == null) {
            System.out.println("failed");
            System.out.println("Sorry, we cannot create user with null argument.");
            System.out.println("Please, create user again :)");
            return -1;
        }
        System.out.println("ok");
        return 0;
    }

    public int clearUser() {
        System.out.println("Remove all users");
        userService.clear();
        System.out.println("ok");
        return 0;
    }

    public int listUser() {
        System.out.println("Users list");
        int index = 1;
        for (final User user : userService.findAll()) {
            System.out.println(index + ". " + user.getId() + ": " + user.getLogin() + ", " + user.getRole());
            index++;
        }
        System.out.println("ok");
        return 0;
    }

    public int viewUserByLogin() {
        System.out.println("Please, enter user login:");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            System.out.println("failed");
            System.out.println("Sorry, we cannot display user with this login or null argument.");
            return 0;
        }
        viewUser(user);
        return 0;
    }

    public int updateUserByLogin() {
        System.out.println("Update user by login");
        System.out.println("Please, enter user login:");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            System.out.println("failed");
            System.out.println("Sorry, we cannot updater user with this login or null argument.");
            return 0;
        }
        System.out.println("Please, enter new password:");
        final String password = scanner.nextLine();
        System.out.println("Please, enter new role:");
        final String role = scanner.nextLine();
        System.out.println("Please, enter new first name:");
        final String firstName = scanner.nextLine();
        System.out.println("Please, enter new last name:");
        final String lastName = scanner.nextLine();
        userService.update(login, role, password, firstName, lastName);
        System.out.println("ok");
        return 0;
    }

    public int addUserInformation() {
        System.out.println("Add user information");
        System.out.println("Please, enter user login:");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            System.out.println("failed");
            System.out.println("Sorry, we cannot add user info with this login or null argument.");
            return 0;
        }
        System.out.println("Please, enter your first name:");
        final String firstName = scanner.nextLine();
        System.out.println("Please, enter your last name:");
        final String lastName = scanner.nextLine();
        userService.addUserInformation(login, firstName, lastName);
        System.out.println("ok");
        return 0;
    }

    public int removeUserByLogin() {
        System.out.println("Remove user");
        System.out.println("Please, enter user login:");
        final String login = scanner.nextLine();
        final User user = userService.removeByLogin(login);
        if (user == null) {
            System.out.println("failed");
        } else {
            System.out.println("ok");
        }
        return 0;
    }

    private void viewUser(final User user) {
        if (user == null) return;
        System.out.println("View user");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("PASSWORD: " + user.getPassword());
        System.out.println("ROLE: " + user.getRole());
        System.out.println();
        System.out.println("FIRST_NAME: " + user.getFirstName());
        System.out.println("LAST_NAME: " + user.getLastName());
        System.out.println("ok");
    }

    private String grantRoles() {
        System.out.println("If you want to install the admin role, enter - ADMIN");
        System.out.println("All new users are assigned the USER role by default");
        final String role = scanner.nextLine();
        return role;
    }

}