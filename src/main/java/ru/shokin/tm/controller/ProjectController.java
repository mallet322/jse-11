package ru.shokin.tm.controller;

import ru.shokin.tm.entity.Project;
import ru.shokin.tm.service.ProjectService;
import ru.shokin.tm.service.ProjectTaskService;

public class ProjectController extends AbstractController {

    private final ProjectService projectService;

    private final ProjectTaskService projectTaskService;

    public ProjectController(ProjectService projectService, ProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    public int createProject() {
        System.out.println("Create project");
        System.out.println("Please, enter project name:");
        final String name = scanner.nextLine();
        System.out.println("Please, enter project description:");
        final String description = scanner.nextLine();
        if (projectService.create(name, description) == null) {
            System.out.println("failed");
            System.out.println("Sorry, we cannot create project with null argument.");
            System.out.println("Please, create project again :)");
            return -1;
        }
        System.out.println("ok");
        return 0;
    }

    public int clearProject() {
        System.out.println("Clear project");
        projectService.clear();
        System.out.println("ok");
        return 0;
    }

    public int listProject() {
        System.out.println("List project");
        int index = 1;
        for (final Project project : projectService.findAll()) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName());
            index++;
        }
        System.out.println("ok");
        return 0;
    }

    public void viewProject(final Project project) {
        if (project == null) return;
        System.out.println("View project");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("ok");
    }

    public int viewProjectByIndex() {
        System.out.println("Please, enter project index:");
        final int index = scanner.nextInt() - 1;
        final Project project = projectService.findByIndex(index);
        viewProject(project);
        return 0;
    }

    public int viewProjectById() {
        System.out.println("Please, enter project id:");
        final long id = scanner.nextLong();
        final Project project = projectService.findById(id);
        viewProject(project);
        return 0;
    }

    public int updateProjectByIndex() {
        System.out.println("Update project by index");
        System.out.println("Please, enter project index:");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("failed");
            return 0;
        }
        System.out.println("Please, enter project name:");
        final String name = scanner.nextLine();
        System.out.println("Please, enter project description:");
        final String description = scanner.nextLine();
        projectService.update(project.getId(), name, description);
        System.out.println("ok");
        return 0;
    }

    public int updateProjectById() {
        System.out.println("Update project by id");
        System.out.println("Please, enter project id:");
        final long id = Long.parseLong(scanner.nextLine());
        final Project project = projectService.findById(id);
        if (project == null) {
            System.out.println("failed");
            return 0;
        }
        System.out.println("Please, enter project name:");
        final String name = scanner.nextLine();
        System.out.println("Please, enter project description:");
        final String description = scanner.nextLine();
        projectService.update(id, name, description);
        System.out.println("ok");
        return 0;
    }

    public int removeProjectByIndex() {
        System.out.println("Remove project by index");
        System.out.println("Please, enter project index:");
        final int index = scanner.nextInt() - 1;
        final Project project = projectService.removeByIndex(index);
        if (project == null) {
            System.out.println("failed");
        } else {
            final long projectId = project.getId();
            projectTaskService.removeProjectWithTasks(projectId);
            System.out.println("ok");
        }
        return 0;
    }

    public int removeProjectById() {
        System.out.println("Remove project by id");
        System.out.println("Please, enter project id:");
        final long id = scanner.nextLong();
        final Project project = projectService.removeById(id);
        if (project == null) {
            System.out.println("failed");
        } else {
            projectTaskService.removeProjectWithTasks(id);
            System.out.println("ok");
        }
        return 0;
    }

    public int removeProjectByName() {
        System.out.println("Remove project by name");
        System.out.println("Please, enter project name:");
        final String name = scanner.nextLine();
        final Project project = projectService.removeByName(name);
        if (project == null) {
            System.out.println("failed");
        } else {
            final long projectId = project.getId();
            projectTaskService.removeProjectWithTasks(projectId);
            System.out.println("ok");
        }
        return 0;
    }

}