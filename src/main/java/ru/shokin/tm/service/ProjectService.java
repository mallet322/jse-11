package ru.shokin.tm.service;

import ru.shokin.tm.entity.Project;
import ru.shokin.tm.repository.ProjectRepository;

import java.util.List;

public class ProjectService {

    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.create(name, description);
    }

    public Project update(final Long id, final String name, final String description) {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.update(id, name, description);
    }

    public void clear() {
        projectRepository.clear();
    }

    public Project removeByIndex(final int index) {
        if (index < 0 || index > projectRepository.findAll().size() - 1) return null;
        return projectRepository.removeByIndex(index);
    }

    public Project removeById(final Long id) {
        if (id == null) return null;
        return projectRepository.removeById(id);
    }

    public Project removeByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.removeByName(name);
    }

    public Project findByIndex(final int index) {
        if (index < 0 || index > projectRepository.findAll().size() - 1) return null;
        return projectRepository.findByIndex(index);
    }

    public Project findById(final Long id) {
        if (id == null) return null;
        return projectRepository.findById(id);
    }

    public Project findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findByName(name);
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

}