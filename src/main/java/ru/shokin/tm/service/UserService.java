package ru.shokin.tm.service;


import ru.shokin.tm.constant.Role;
import ru.shokin.tm.entity.User;
import ru.shokin.tm.repository.UserRepository;

import java.util.List;

public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User create(final String login, final String password, final String role) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        if (role == null) return null;
        final Role userRole = getUserRole(role);
        return userRepository.create(login, password, userRole);
    }

    public User update(final String login, final String role, final String password,
                       final String firstName, final String lastName) {
        if (login == null || login.isEmpty()) return null;
        if (role == null) return null;
        if (password == null || password.isEmpty()) return null;
        if (firstName == null || firstName.isEmpty()) return null;
        if (lastName == null || lastName.isEmpty()) return null;
        final Role userRole = getUserRole(role);
        return userRepository.update(login, password, userRole, firstName, lastName);
    }

    private Role getUserRole(final String role) {
        try {
            return Role.valueOf(role);
        } catch (IllegalArgumentException | NullPointerException exception) {
            System.out.println("Role does not exist.");
            System.out.println("USER role set by default");
            return Role.USER;
        }
    }

    public User addUserInformation(final String login, final String firstName, final String lastName) {
        if (login == null || login.isEmpty()) return null;
        if (firstName == null || firstName.isEmpty()) return null;
        if (lastName == null || lastName.isEmpty()) return null;
        return userRepository.addUserInformation(login, firstName, lastName);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.findByLogin(login);
    }

    public void clear() {
        userRepository.clear();
    }

    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.removeByLogin(login);
    }

}