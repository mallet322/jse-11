package ru.shokin.tm;

import static ru.shokin.tm.constant.TerminalConst.*;

import ru.shokin.tm.constant.Role;
import ru.shokin.tm.repository.ProjectRepository;
import ru.shokin.tm.repository.TaskRepository;
import ru.shokin.tm.repository.UserRepository;
import ru.shokin.tm.controller.SystemController;
import ru.shokin.tm.controller.ProjectController;
import ru.shokin.tm.controller.TaskController;
import ru.shokin.tm.controller.UserController;
import ru.shokin.tm.service.ProjectService;
import ru.shokin.tm.service.ProjectTaskService;
import ru.shokin.tm.service.TaskService;
import ru.shokin.tm.service.UserService;

import java.util.Scanner;

public class Application {

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final TaskRepository taskRepository = new TaskRepository();

    private final UserRepository userRepository = new UserRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final TaskService taskService = new TaskService(taskRepository);

    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final UserService userService = new UserService(userRepository);

    private final SystemController systemController = new SystemController();

    private final ProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final TaskController taskController = new TaskController(taskService, projectTaskService);

    private final UserController userController = new UserController(userService);

    {
        projectRepository.create("DEMO PROJECT 1", "PROJECT DESC 1");
        projectRepository.create("DEMO PROJECT 2", "PROJECT DESC 2");
        taskRepository.create("DEMO TASK 1", "TASK DESC 1");
        taskRepository.create("DEMO TASK 2", "TASK DESC 2");
        userRepository.create("ADMIN", "Admin_p@ss321", Role.ADMIN);
        userRepository.create("TEST", "User_p@ss123", Role.USER);

    }

    public static void main(final String[] args) {
        final Application app = new Application();
        app.run(args);
        app.systemController.displayWelcome();
        app.process();
    }

    private void process() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            run(command);
            System.out.println();
        }
    }

    private void run(final String[] args) {
        if (args == null || args.length < 1) return;

        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    private int run(final String command) {
        if (command == null || command.isEmpty()) return -1;
        switch (command) {
            case VERSION:
                return systemController.displayVersion();
            case ABOUT:
                return systemController.displayAbout();
            case HELP:
                return systemController.displayHelp();
            case EXIT:
                return systemController.displayExit();

            case PROJECT_CREATE:
                return projectController.createProject();
            case PROJECT_CLEAR:
                return projectController.clearProject();
            case PROJECT_LIST:
                return projectController.listProject();
            case PROJECT_VIEW_BY_INDEX:
                return projectController.viewProjectByIndex();
            case PROJECT_VIEW_BY_ID:
                return projectController.viewProjectById();
            case PROJECT_REMOVE_BY_INDEX:
                return projectController.removeProjectByIndex();
            case PROJECT_REMOVE_BY_NAME:
                return projectController.removeProjectByName();
            case PROJECT_REMOVE_BY_ID:
                return projectController.removeProjectById();
            case PROJECT_UPDATE_BY_INDEX:
                return projectController.updateProjectByIndex();
            case PROJECT_UPDATE_BY_ID:
                return projectController.updateProjectById();

            case TASK_CREATE:
                return taskController.createTask();
            case TASK_CLEAR:
                return taskController.clearTask();
            case TASK_LIST:
                return taskController.listTask();
            case TASK_VIEW_BY_INDEX:
                return taskController.viewTaskByIndex();
            case TASK_VIEW_BY_ID:
                return taskController.viewTaskById();
            case TASK_REMOVE_BY_INDEX:
                return taskController.removeTaskByIndex();
            case TASK_REMOVE_BY_NAME:
                return taskController.removeTaskByName();
            case TASK_REMOVE_BY_ID:
                return taskController.removeTaskById();
            case TASK_UPDATE_BY_INDEX:
                return taskController.updateTaskByIndex();
            case TASK_UPDATE_BY_ID:
                return taskController.updateTaskById();
            case TASK_ADD_TO_PROJECT_BY_IDS:
                return taskController.addTaskToProjectByIds();
            case TASK_REMOVE_FROM_PROJECT_BY_IDS:
                return taskController.removeTaskFromProjectByIds();
            case TASK_LIST_BY_PROJECT_ID:
                return taskController.listTaskByProjectId();

            case USER_CREATE:
                return userController.createUser();
            case USER_CLEAR:
                return userController.clearUser();
            case USER_LIST:
                return userController.listUser();
            case USER_ADD_INFORMATION:
                return userController.addUserInformation();
            case USER_VIEW_BY_LOGIN:
                return userController.viewUserByLogin();
            case USER_REMOVE_BY_LOGIN:
                return userController.removeUserByLogin();
            case USER_UPDATE_BY_LOGIN:
                return userController.updateUserByLogin();

            default:
                return systemController.displayError();
        }
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public ProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

}